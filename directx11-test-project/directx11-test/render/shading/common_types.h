#pragma once

namespace xtest {
namespace render {
namespace shading {

	// used to bind a cbuffer to a shader 
	// warning: the value of the enum is used to select the shader slot
	enum class CBufferFrequency
	{
		per_object = 0,
		per_frame = 1,
		rarely_changed = 2,
		unknown

	};


	// used to bind a resource to the corrisponding shader
	enum class ShaderTarget
	{
		vertex_shader,
		pixel_shader,
		unknown
	};


	// used to bind a texture to a shader
	// warning: the value of the enum is used to select the shader slot
	enum class TextureUsage
	{
		color = 0,
		normal = 1,
		glossiness = 2,
		shadow_map = 10,
		shadow_map_spot_light = 11,
		projector_map = 20,
		point_light_1 = 21,
		point_light_2 = 22,
		point_light_3 = 23,
		point_light_4 = 24,
		point_light_5 = 25,
		point_light_6 = 26,
		uknown
	};


	// used to bind a texture sampler to a shader
	// warning: the value of the enum is used to select the shader slot
	enum class SamplerUsage
	{
		common_textures = 0,
		shadow_map = 10,
		unknown
	};


} // shading
} // render
} // xtest
