#include "stdafx.h"
#include "shadows_demo_app.h"
#include <file/file_utils.h>
#include <math/math_utils.h>
#include <service/locator.h>
#include <render/shading/vertex_input_types.h>
#include <render/shading/rasterizer_state_types.h>
#include <render/shading/sampler_types.h>
#include <external_libs/directxtk/WICTextureLoader.h>


using namespace DirectX;
using namespace xtest;
using namespace xtest::render::shading;

using xtest::demo::ShadowsDemoApp;
using Microsoft::WRL::ComPtr;

ShadowsDemoApp::ShadowsDemoApp(HINSTANCE instance,
	const application::WindowSettings& windowSettings,
	const application::DirectxSettings& directxSettings,
	uint32 fps /*=60*/)
	: application::DirectxApp(instance, windowSettings, directxSettings, fps)
	, m_dirKeyLight()
	, m_dirFillLight()
	, m_spotLight()
	, m_pointLight()
	, m_lightingControls()
	, m_isLightingControlsDirty(true)
	, m_stopLights(false)
	, m_camera(math::ToRadians(60.f), math::ToRadians(125.f), 5.f, { 0.f, 0.f, 0.f }, { 0.f, 1.f, 0.f }, { math::ToRadians(4.f), math::ToRadians(175.f) }, { 3.f, 25.f })
	, m_objects()
	, m_dirLightRenderPass()
	, m_drawRenderPass()
	// temp bounding sphere values
	, m_boundingSphere(math::ToRadians(1.f), math::ToRadians(1.f), 1.f, { 0.f, 0.f, 0.f }, { 0.f, 1.f, 0.f }, math::ClampValues<float>(math::ToRadians(1.f), math::ToRadians(179.f)), math::ClampValues<float>(1.f, 100.f))
{}

ShadowsDemoApp::~ShadowsDemoApp()
{}


void ShadowsDemoApp::Init()
{
	application::DirectxApp::Init();

	m_camera.SetPerspectiveProjection(math::ToRadians(45.f), AspectRatio(), 1.f, 1000.f);

	InitShadows();
	InitRenderTechnique();
	InitRenderables();
	InitLights();

	service::Locator::GetMouse()->AddListener(this);
	service::Locator::GetKeyboard()->AddListener(this, { input::Key::F, input::Key::F1, input::Key::F2, input::Key::F3, input::Key::space_bar });
}


void ShadowsDemoApp::InitRenderTechnique()
{
	file::ResourceLoader* loader = service::Locator::GetResourceLoader();

	// Render scene render pass

	m_vertexShader = std::make_shared<VertexShader>(loader->LoadBinaryFile(GetRootDir().append(L"\\shadows_demo_VS_2.cso")));
	m_vertexShader->SetVertexInput(std::make_shared<MeshDataVertexInput>());
	m_vertexShader->AddConstantBuffer(CBufferFrequency::per_object, std::make_unique<CBuffer<PerObjectData>>());

	m_pixelShader = std::make_shared<PixelShader>(loader->LoadBinaryFile(GetRootDir().append(L"\\shadows_demo_PS.cso")));
	m_pixelShader->AddConstantBuffer(CBufferFrequency::per_object, std::make_unique<CBuffer<PerObjectData>>());
	m_pixelShader->AddConstantBuffer(CBufferFrequency::per_frame, std::make_unique<CBuffer<PerFrameData>>());
	m_pixelShader->AddConstantBuffer(CBufferFrequency::rarely_changed, std::make_unique<CBuffer<RarelyChangedData>>());
	m_pixelShader->AddSampler(SamplerUsage::common_textures, std::make_shared<AnisotropicSampler>());
	m_pixelShader->AddSampler(SamplerUsage::shadow_map, std::make_shared<PCFSampler>());

	m_drawRenderPass.SetState(std::make_shared<RenderPassState>(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST, m_viewport, std::make_shared<SolidCullBackRS>(), m_backBufferView.Get(), m_depthBufferView.Get()));
	m_drawRenderPass.SetVertexShader(m_vertexShader);
	m_drawRenderPass.SetPixelShader(m_pixelShader);
	m_drawRenderPass.Init();


	// Directional light render pass

	m_vertexShaderShadows = std::make_shared<VertexShader>(loader->LoadBinaryFile(GetRootDir().append(L"\\shadows_demo_VS_1.cso")));
	m_vertexShaderShadows->SetVertexInput(std::make_shared<PosOnlyVertexInput>());
	m_vertexShaderShadows->AddConstantBuffer(CBufferFrequency::per_object, std::make_unique<CBuffer<PerObjectDataShadow>>());

	m_pixelShaderShadows = nullptr;

	m_dirLightRenderPass.SetState(std::make_shared<RenderPassState>(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST, m_viewportShadows, std::make_shared<SolidCullBackDepthBiasRS>(), nullptr, m_depthStencilViewShadowsDirLight.Get()));
	m_dirLightRenderPass.SetVertexShader(m_vertexShaderShadows);
	m_dirLightRenderPass.SetPixelShader(m_pixelShaderShadows);
	m_dirLightRenderPass.Init();

	// Spot light render pass

	m_spotLightRenderPass.SetState(std::make_shared<RenderPassState>(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST, m_viewportShadows, std::make_shared<SolidCullBackDepthBiasRS>(), nullptr, m_depthStencilViewShadowsSpotLight.Get()));
	m_spotLightRenderPass.SetVertexShader(m_vertexShaderShadows);
	m_spotLightRenderPass.SetPixelShader(m_pixelShaderShadows);
	m_spotLightRenderPass.Init();

	// Point light render pass
	for (int i = 0; i < m_pointLightRenderPass.size(); i++) 
	{
		m_pointLightRenderPass[i].SetState(std::make_shared<RenderPassState>(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST, m_viewportShadows, std::make_shared<SolidCullBackDepthBiasRS>(), nullptr, m_depthStencilViewPointLight[i].Get()));
		m_pointLightRenderPass[i].SetVertexShader(m_vertexShaderShadows);
		m_pointLightRenderPass[i].SetPixelShader(m_pixelShaderShadows);
		m_pointLightRenderPass[i].Init();
	}
}


void ShadowsDemoApp::InitRenderables()
{
	//ground
	{
		render::Renderable plane{ *(service::Locator::GetResourceLoader()->LoadGPFMesh(GetRootDir().append(LR"(\3d-objects\rocks_dorama\rocks_composition.gpf)"))) };
		plane.SetTransform(XMMatrixTranslation(0.f, 0.f, 0.f));
		plane.Init();
		m_objects.push_back(std::move(plane));
	}

	// pg
	{
		render::Renderable pg{ *(service::Locator::GetResourceLoader()->LoadGPFMesh(GetRootDir().append(LR"(\3d-objects\gdc_female\gdc_female_posed_2.gpf)"))) };
		pg.SetTransform(XMMatrixTranslation(0.f, 0.3f, 0.f));
		pg.Init();
		m_objects.push_back(std::move(pg));
	}

}


void ShadowsDemoApp::InitLights()
{
	XMVECTOR dirVec = XMVectorSet(5.f, 4.f, 5.f, 0.f);
	m_dirKeyLight.ambient = { 0.16f, 0.18f, 0.18f, 1.f };
	m_dirKeyLight.diffuse = { 2.f * 0.78f, 2.f * 0.83f, 2.f * 1.f, 1.f };
	m_dirKeyLight.specular = { 0.87f,  0.90f,  0.94f, 1.f };
	XMVECTOR dirLightDirection = XMVector3Normalize(-dirVec);
	XMStoreFloat3(&m_dirKeyLight.dirW, dirLightDirection);
	// directional light bounding sphere values
	m_boundingSphere.SetRotation(acos(-m_dirKeyLight.dirW.y), atan(m_dirKeyLight.dirW.z / m_dirKeyLight.dirW.x));
	m_boundingSphere.SetRadius(35.f);

	m_pointLight.ambient = { 0.16f, 0.18f, 0.18f, 1.f };
	m_pointLight.diffuse = { 2.f * 0.78f, 2.f * 0.83f, 2.f * 1.f, 1.f };
	m_pointLight.specular = { 0.87f,  0.90f,  0.94f, 1.f };
	m_pointLight.posW = { -3.5f, 5.f, 3.5f };
	m_pointLight.range = 40.f;
	m_pointLight.attenuation = { 0.0f, 0.2f, 0.f };


	m_spotLight.ambient = { 0.16f, 0.18f, 0.18f, 1.f };
	m_spotLight.diffuse = { 2.f * 0.78f, 2.f * 0.83f, 2.f * 1.f, 1.f };
	m_spotLight.specular = { 0.87f,  0.90f,  0.94f, 1.f };
	m_spotLight.posW = { 4.f, 10.f, 8.f };
	m_spotLight.range = 80.f;
	XMVECTOR dirW = XMVector3Normalize(XMVectorSet(0.f, -1.f, -1.f, 1.f));
	XMStoreFloat3(&m_spotLight.dirW, dirW);
	m_spotLight.spot = 80.f;
	m_spotLight.attenuation = { 0.0f, 0.125f, 0.f };

	// spotlight projected texture
	{
		std::wstring texturesFolder = GetRootDir().append(LR"(\3d-objects)");
		std::wstring textureName = LR"(\sci-fi\sci_fi)";
		std::wstring completePath = texturesFolder + textureName + LR"(_color.png)";
		xtest::file::ResourceLoader::LoadedTexture texture = service::Locator::GetResourceLoader()->LoadTexture(completePath);
		m_projectorTexture = std::move(texture.d3dShaderView);
	}


	m_lightingControls.useDirLight = true;
	m_lightingControls.usePointLight = true;
	m_lightingControls.useSpotLight = true;
}


void ShadowsDemoApp::InitShadows() 
{
	CD3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;

	textureDesc.Width = (UINT)m_shadowResolution;
	textureDesc.Height = (UINT)m_shadowResolution;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;

	XTEST_D3D_CHECK(m_d3dDevice->CreateTexture2D(&textureDesc, nullptr, &m_textureShadowsDirLight));
	XTEST_D3D_CHECK(m_d3dDevice->CreateTexture2D(&textureDesc, nullptr, &m_textureShadowsSpotLight));
	for (int i = 0; i < m_texturePointLight.size(); i++)
	{
		XTEST_D3D_CHECK(m_d3dDevice->CreateTexture2D(&textureDesc, nullptr, &m_texturePointLight[i]));
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

	depthStencilViewDesc.Flags = 0;
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	XTEST_D3D_CHECK(m_d3dDevice->CreateDepthStencilView(m_textureShadowsDirLight.Get(), &depthStencilViewDesc, &m_depthStencilViewShadowsDirLight));
	XTEST_D3D_CHECK(m_d3dDevice->CreateDepthStencilView(m_textureShadowsSpotLight.Get(), &depthStencilViewDesc, &m_depthStencilViewShadowsSpotLight));
	for (int i = 0; i < m_depthStencilViewPointLight.size(); i++)
	{
		XTEST_D3D_CHECK(m_d3dDevice->CreateDepthStencilView(m_texturePointLight[i].Get(), &depthStencilViewDesc, &m_depthStencilViewPointLight[i]));
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderViewDesc;
	shaderViewDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	shaderViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderViewDesc.Texture2D.MipLevels = 1;
	shaderViewDesc.Texture2D.MostDetailedMip = 0;

	XTEST_D3D_CHECK(m_d3dDevice->CreateShaderResourceView(m_textureShadowsDirLight.Get(), &shaderViewDesc, &m_shaderViewShadowsDirLight));
	XTEST_D3D_CHECK(m_d3dDevice->CreateShaderResourceView(m_textureShadowsSpotLight.Get(), &shaderViewDesc, &m_shaderViewShadowsSpotLight));
	for (int i = 0; i < m_shaderViewPointLight.size(); i++) {
		XTEST_D3D_CHECK(m_d3dDevice->CreateShaderResourceView(m_texturePointLight[i].Get(), &shaderViewDesc, &m_shaderViewPointLight[i]));

	}

	m_viewportShadows.TopLeftX = 0.f;
	m_viewportShadows.TopLeftY = 0.f;
	m_viewportShadows.Width = m_shadowResolution;
	m_viewportShadows.Height = m_shadowResolution;
	m_viewportShadows.MinDepth = 0.f;
	m_viewportShadows.MaxDepth = 1.f;

}


void ShadowsDemoApp::OnResized()
{
	application::DirectxApp::OnResized();

	//update the render pass state with the resized render target and depth buffer
	m_drawRenderPass.GetState()->ChangeRenderTargetView(m_backBufferView.Get());
	m_drawRenderPass.GetState()->ChangeDepthStencilView(m_depthBufferView.Get());
	m_drawRenderPass.GetState()->ChangeViewPort(m_viewport);

	//update the projection matrix with the new aspect ratio
	m_camera.SetPerspectiveProjection(math::ToRadians(45.f), AspectRatio(), 1.f, 1000.f);
}


void ShadowsDemoApp::OnWheelScroll(input::ScrollStatus scroll)
{
	// move forward/backward when the wheel is used
	if (service::Locator::GetMouse()->IsInClientArea())
	{
		XMFLOAT3 cameraZ = m_camera.GetZAxis();
		XMFLOAT3 forwardMovement;
		XMStoreFloat3(&forwardMovement, XMVectorScale(XMLoadFloat3(&cameraZ), scroll.isScrollingUp ? 0.5f : -0.5f));
		m_camera.TranslatePivotBy(forwardMovement);
	}
}


void ShadowsDemoApp::OnMouseMove(const DirectX::XMINT2& movement, const DirectX::XMINT2& currentPos)
{
	XTEST_UNUSED_VAR(currentPos);

	input::Mouse* mouse = service::Locator::GetMouse();

	// rotate the camera position around the cube when the left button is pressed
	if (mouse->GetButtonStatus(input::MouseButton::left_button).isDown && mouse->IsInClientArea())
	{
		m_camera.RotateBy(math::ToRadians(movement.y * -0.25f), math::ToRadians(movement.x * 0.25f));
	}

	// pan the camera position when the right button is pressed
	if (mouse->GetButtonStatus(input::MouseButton::right_button).isDown && mouse->IsInClientArea())
	{
		XMFLOAT3 cameraX = m_camera.GetXAxis();
		XMFLOAT3 cameraY = m_camera.GetYAxis();

		// we should calculate the right amount of pan in screen space but for now this is good enough
		XMVECTOR xPanTranslation = XMVectorScale(XMLoadFloat3(&cameraX), float(-movement.x) * 0.01f);
		XMVECTOR yPanTranslation = XMVectorScale(XMLoadFloat3(&cameraY), float(movement.y) * 0.01f);

		XMFLOAT3 panTranslation;
		XMStoreFloat3(&panTranslation, XMVectorAdd(xPanTranslation, yPanTranslation));
		m_camera.TranslatePivotBy(panTranslation);
	}

}


void ShadowsDemoApp::OnKeyStatusChange(input::Key key, const input::KeyStatus& status)
{

	// re-frame F key is pressed
	if (key == input::Key::F && status.isDown)
	{
		m_camera.SetPivot({ 0.f, 0.f, 0.f });
	}
	else if (key == input::Key::F1 && status.isDown)
	{
		m_lightingControls.useDirLight = !m_lightingControls.useDirLight;
		m_isLightingControlsDirty = true;
	}
	else if (key == input::Key::F2 && status.isDown)
	{
		m_lightingControls.usePointLight = !m_lightingControls.usePointLight;
		m_isLightingControlsDirty = true;
	}
	else if (key == input::Key::F3 && status.isDown)
	{
		m_lightingControls.useSpotLight = !m_lightingControls.useSpotLight;
		m_isLightingControlsDirty = true;
	}
	else if (key == input::Key::space_bar && status.isDown)
	{
		m_stopLights = !m_stopLights;
	}
}


void ShadowsDemoApp::UpdateScene(float deltaSeconds)
{

	// PerFrameCB
	{
		// directional light bounding sphere values
		m_boundingSphere.SetRotation(acos(-m_dirKeyLight.dirW.y), atan(m_dirKeyLight.dirW.z / m_dirKeyLight.dirW.x));

		PerFrameData data;
		data.dirLights[0] = m_dirKeyLight;
		data.spotLight = m_spotLight;
		data.pointLights[0] = m_pointLight;
		//data.pointLights[0].posW = m_pointLight.posW;
		data.eyePosW = m_camera.GetPosition();

		m_drawRenderPass.GetPixelShader()->GetConstantBuffer(CBufferFrequency::per_frame)->UpdateBuffer(data);
	}


	// RarelyChangedCB
	if (m_isLightingControlsDirty)
	{
		m_drawRenderPass.GetPixelShader()->GetConstantBuffer(CBufferFrequency::rarely_changed)->UpdateBuffer(m_lightingControls);
		m_isLightingControlsDirty = false;
	}

}


void ShadowsDemoApp::RenderScene()
{
	m_d3dAnnotation->BeginEvent(L"render-scene");

	if (m_lightingControls.useDirLight) {
		m_dirLightRenderPass.Bind();
		m_dirLightRenderPass.GetState()->ClearDepthOnly();
		// draw shadows
		for (render::Renderable& renderable : m_objects)
		{
			for (const std::string& meshName : renderable.GetMeshNames())
			{
				PerObjectDataShadow data = ToPerObjectDataDirLight(renderable, meshName);
				m_dirLightRenderPass.GetVertexShader()->GetConstantBuffer(CBufferFrequency::per_object)->UpdateBuffer(data);
				renderable.Draw(meshName);
			}
		}
	}

	if (m_lightingControls.useSpotLight) {
		m_spotLightRenderPass.Bind();
		m_spotLightRenderPass.GetState()->ClearDepthOnly();
		// draw shadows
		for (render::Renderable& renderable : m_objects)
		{
			for (const std::string& meshName : renderable.GetMeshNames())
			{
				PerObjectDataShadow data = ToPerObjectDataSpotLight(renderable, meshName);
				m_spotLightRenderPass.GetVertexShader()->GetConstantBuffer(CBufferFrequency::per_object)->UpdateBuffer(data);
				renderable.Draw(meshName);
			}
		}
	}

	if (m_lightingControls.usePointLight) {
		for (int i = 0; i < m_pointLightRenderPass.size(); i++) {
			m_pointLightRenderPass[i].Bind();
			m_pointLightRenderPass[i].GetState()->ClearDepthOnly();
			// draw shadows
			for (render::Renderable& renderable : m_objects)
			{
				for (const std::string& meshName : renderable.GetMeshNames())
				{
					PerObjectDataShadow data = ToPerObjectDataPointLight(renderable, meshName, i);
					m_pointLightRenderPass[i].GetVertexShader()->GetConstantBuffer(CBufferFrequency::per_object)->UpdateBuffer(data);
					renderable.Draw(meshName);
				}
			}
		}
	}


	m_drawRenderPass.Bind();
	m_drawRenderPass.GetState()->ClearDepthOnly();
	m_drawRenderPass.GetState()->ClearRenderTarget(DirectX::Colors::DarkGray);

	// draw objects
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::shadow_map, m_shaderViewShadowsDirLight.Get());
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::shadow_map_spot_light, m_shaderViewShadowsSpotLight.Get());
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::projector_map, m_projectorTexture.Get());
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_1, m_shaderViewPointLight[0].Get());
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_2, m_shaderViewPointLight[1].Get());
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_3, m_shaderViewPointLight[2].Get());
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_4, m_shaderViewPointLight[3].Get());
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_5, m_shaderViewPointLight[4].Get());
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_6, m_shaderViewPointLight[5].Get());

	for (render::Renderable& renderable : m_objects)
	{
		for (const std::string& meshName : renderable.GetMeshNames())
		{
			PerObjectData data = ToPerObjectData(renderable, meshName);
			m_drawRenderPass.GetVertexShader()->GetConstantBuffer(CBufferFrequency::per_object)->UpdateBuffer(data);
			m_drawRenderPass.GetPixelShader()->GetConstantBuffer(CBufferFrequency::per_object)->UpdateBuffer(data);
			m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::color, renderable.GetTextureView(TextureUsage::color, meshName));
			m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::normal, renderable.GetTextureView(TextureUsage::normal, meshName));
			m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::glossiness, renderable.GetTextureView(TextureUsage::glossiness, meshName));
			renderable.Draw(meshName);
		}
	}

	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::shadow_map, nullptr);
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::shadow_map_spot_light, nullptr);
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::projector_map, nullptr);
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_1, nullptr);
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_2, nullptr);
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_3, nullptr);
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_4, nullptr);
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_5, nullptr);
	m_drawRenderPass.GetPixelShader()->BindTexture(TextureUsage::point_light_6, nullptr);

	XTEST_D3D_CHECK(m_swapChain->Present(0, 0));

	m_d3dAnnotation->EndEvent();
}


ShadowsDemoApp::PerObjectData ShadowsDemoApp::ToPerObjectData(const render::Renderable& renderable, const std::string& meshName) const
{
	PerObjectData data;

	XMMATRIX W = XMLoadFloat4x4(&renderable.GetTransform());
	XMMATRIX transform = XMLoadFloat4x4(&renderable.GetTexcoordTransform(meshName));
	XMMATRIX V = m_camera.GetViewMatrix();
	XMMATRIX P = m_camera.GetProjectionMatrix();
	XMMATRIX WVP = W * V * P;

	XMStoreFloat4x4(&data.W, XMMatrixTranspose(W));
	XMStoreFloat4x4(&data.WVP, XMMatrixTranspose(WVP));
	XMStoreFloat4x4(&data.W_inverseTraspose, XMMatrixInverse(nullptr, W));
	XMStoreFloat4x4(&data.TexcoordMatrix, XMMatrixTranspose(transform));

	XMMATRIX T = {
	0.5f,  0.f , 0.f, 0.f,
	0.0f, -0.5f, 0.f, 0.f,
	0.0f,  0.f , 1.f, 0.f,
	0.5f,  0.5f, 0.f, 1.f,
	};

	// Directional light
	{
		XMMATRIX VL = m_boundingSphere.GetViewMatrix();

		XMMATRIX PL = XMMatrixOrthographicOffCenterLH(
			- m_boundingSphere.GetRadius(),
			+ m_boundingSphere.GetRadius(),
			- m_boundingSphere.GetRadius(),
			+ m_boundingSphere.GetRadius(),
			0,
			2.f * m_boundingSphere.GetRadius()
		);

		XMStoreFloat4x4(&data.WVPT_shadowMap, XMMatrixTranspose(W * VL * PL * T));
	}

	// Spot light
	{
		XMVECTOR focusPoint = XMLoadFloat3(&m_spotLight.dirW) * m_spotLight.range - XMLoadFloat3(&m_spotLight.posW);
		XMMATRIX VL = XMMatrixLookAtLH(XMLoadFloat3(&m_spotLight.posW), focusPoint, XMVectorSet(0.f, 1.f, 0.f, 0.f));

		XMMATRIX PL = XMMatrixPerspectiveFovLH(math::ToRadians(45.f), 1.f, 1.f, m_spotLight.range);

		XMStoreFloat4x4(&data.WVPT_shadowMapSpotLight, XMMatrixTranspose(W * VL * PL * T));
	}

	// Point light
	{
		for (int i = 0; i < m_pointLightRenderPass.size(); i++) {

			XMFLOAT3 dirW;
			XMFLOAT3 upDir;
			switch (i) {
				case 0:
					dirW = XMFLOAT3(1.f, 0.f, 0.f);
					upDir = XMFLOAT3(0.f, 1.f, 0.f);
					break;
				case 1:
					dirW = XMFLOAT3(-1.f, 0.f, 0.f);
					upDir = XMFLOAT3(0.f, 1.f, 0.f);
					break;
				case 2:
					dirW = XMFLOAT3(0.f, 1.f, 0.f);
					upDir = XMFLOAT3(0.f, 0.f, 1.f);
					break;
				case 3:
					dirW = XMFLOAT3(0.f, -1.f, 0.f);
					upDir = XMFLOAT3(0.f, 0.f, -1.f);
					break;
				case 4:
					dirW = XMFLOAT3(0.f, 0.f, 1.f);
					upDir = XMFLOAT3(0.f, 1.f, 0.f);
					break;
				case 5:
					dirW = XMFLOAT3(0.f, 0.f, -1.f);
					upDir = XMFLOAT3(0.f, 1.f, 0.f);
					break;
				default:
					XTEST_ASSERT(1 != 1);
					break;
			}

			XMVECTOR focusPoint = XMLoadFloat3(&dirW);
			XMMATRIX VL = XMMatrixLookToLH(XMLoadFloat3(&m_pointLight.posW), focusPoint * m_pointLight.range, XMLoadFloat3(&upDir));

			XMMATRIX PL = XMMatrixPerspectiveFovLH(math::ToRadians(90.05f), 1.f, 1.f, m_pointLight.range);

			XMStoreFloat4x4(&data.WVPT_shadowMapPointLight[i], XMMatrixTranspose(W * VL * PL * T));

		}
		
	}


	data.material.ambient = renderable.GetMaterial(meshName).ambient;
	data.material.diffuse = renderable.GetMaterial(meshName).diffuse;
	data.material.specular = renderable.GetMaterial(meshName).specular;

	return data;
}

ShadowsDemoApp::PerObjectDataShadow ShadowsDemoApp::ToPerObjectDataDirLight(const render::Renderable& renderable, const std::string& meshName) const
{
	PerObjectDataShadow data;

	XMMATRIX V = m_boundingSphere.GetViewMatrix();

	XMMATRIX P = XMMatrixOrthographicOffCenterLH(
		-m_boundingSphere.GetRadius(),
		+m_boundingSphere.GetRadius(),
		-m_boundingSphere.GetRadius(),
		+m_boundingSphere.GetRadius(),
		0,
		2.f * m_boundingSphere.GetRadius()
	);

	XMMATRIX W = XMLoadFloat4x4(&renderable.GetTransform());

	XMMATRIX WVP = W * V * P;

	XMStoreFloat4x4(&data.WVP, XMMatrixTranspose(WVP));

	return data;
}

ShadowsDemoApp::PerObjectDataShadow ShadowsDemoApp::ToPerObjectDataSpotLight(const render::Renderable& renderable, const std::string& meshName) const
{
	PerObjectDataShadow data;

	XMVECTOR focusPoint = XMLoadFloat3(&m_spotLight.dirW) * m_spotLight.range - XMLoadFloat3(&m_spotLight.posW);
	XMMATRIX V = XMMatrixLookAtLH(XMLoadFloat3(&m_spotLight.posW), focusPoint, XMVectorSet(0.f, 1.f, 0.f, 0.f));

	XMMATRIX P = XMMatrixPerspectiveFovLH(math::ToRadians(45.f), 1.f, 1.f, m_spotLight.range);

	XMMATRIX W = XMLoadFloat4x4(&renderable.GetTransform());

	XMMATRIX WVP = W * V * P;

	XMStoreFloat4x4(&data.WVP, XMMatrixTranspose(WVP));

	return data;
}

ShadowsDemoApp::PerObjectDataShadow ShadowsDemoApp::ToPerObjectDataPointLight(const render::Renderable& renderable, const std::string& meshName, const uint32 offset) const
{
	PerObjectDataShadow data;

	XMFLOAT3 dirW;
	XMFLOAT3 upDir;
	switch (offset) {
	case 0:
		dirW = XMFLOAT3(1.f, 0.f, 0.f);
		upDir = XMFLOAT3(0.f, 1.f, 0.f);
		break;
	case 1:
		dirW = XMFLOAT3(-1.f, 0.f, 0.f);
		upDir = XMFLOAT3(0.f, 1.f, 0.f);
		break;
	case 2:
		dirW = XMFLOAT3(0.f, 1.f, 0.f);
		upDir = XMFLOAT3(0.f, 0.f, 1.f);
		break;
	case 3:
		dirW = XMFLOAT3(0.f, -1.f, 0.f);
		upDir = XMFLOAT3(0.f, 0.f, -1.f);
		break;
	case 4:
		dirW = XMFLOAT3(0.f, 0.f, 1.f);
		upDir = XMFLOAT3(0.f, 1.f, 0.f);
		break;
	case 5:
		dirW = XMFLOAT3(0.f, 0.f, -1.f);
		upDir = XMFLOAT3(0.f, 1.f, 0.f);
		break;
	default:
		XTEST_ASSERT(1 != 1);
		break;
	}

	XMVECTOR focusPoint = XMLoadFloat3(&dirW);
	XMMATRIX V = XMMatrixLookToLH(XMLoadFloat3(&m_pointLight.posW), focusPoint * m_pointLight.range, XMLoadFloat3(&upDir));

	XMMATRIX P = XMMatrixPerspectiveFovLH(math::ToRadians(90.05f), 1.f, 1.f, m_pointLight.range);

	XMMATRIX W = XMLoadFloat4x4(&renderable.GetTransform());

	XMMATRIX WVP = W * V * P;

	XMStoreFloat4x4(&data.WVP, XMMatrixTranspose(WVP));

	return data;
}

