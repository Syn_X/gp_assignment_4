
#define POINT_LIGHT_COUNT 1 
#define DIRECTIONAL_LIGHT_COUNT 1


struct Material
{
    float4 ambient;
    float4 diffuse;
    float4 specular;
};

struct DirectionalLight
{
    float4 ambient;
    float4 diffuse;
    float4 specular;
    float3 dirW;
};

struct PointLight
{
    float4 ambient;
    float4 diffuse;
    float4 specular;
    float3 posW;
    float range;
    float3 attenuation;
};

struct SpotLight
{
    float4 ambient;
    float4 diffuse;
    float4 specular;
    float3 posW;
    float range;
    float3 dirW;
    float spot;
    float3 attenuation;
};


struct VertexOut
{
    float4 posH : SV_POSITION;
    float3 posW : POSITION;
    float3 normalW : NORMAL;
    float3 tangentW : TANGENT;
    float2 uv : TEXCOORD;
    float4 shadowPosH : SHADOWPOS;
    float4 shadowPosHSpotLight : SHADOWPOS_SPOT;
    float4 shadowPosHPointLight[6] : SHADOWPOS_POINT;
};

cbuffer PerObjectCB : register(b0)
{
    float4x4 W;
    float4x4 W_inverseTraspose;
    float4x4 WVP;
    float4x4 TexcoordMatrix;
    float4x4 WVPT_shadowMap;
    float4x4 WVPT_shadowMapSpotLight;
    float4x4 WVPT_pointLight[6];
    Material material;
};

cbuffer PerFrameCB : register(b1)
{
    DirectionalLight dirLights[DIRECTIONAL_LIGHT_COUNT];
    PointLight pointLights[POINT_LIGHT_COUNT];
    SpotLight spotLight;
    float3 eyePosW;
};

cbuffer RarelyChangedCB : register(b2)
{
    bool useDirLight;
    bool usePointLight;
    bool useSpotLight;
}

Texture2D diffuseTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D glossTexture : register(t2);
Texture2D shadowMap : register(t10);
Texture2D shadowMapSpotLight : register(t11);
Texture2D projectorTexture : register(t20);
Texture2D pointLightMap_1 : register(t21);
Texture2D pointLightMap_2 : register(t22);
Texture2D pointLightMap_3 : register(t23);
Texture2D pointLightMap_4 : register(t24);
Texture2D pointLightMap_5 : register(t25);
Texture2D pointLightMap_6 : register(t26);

SamplerState textureSampler : register(s0);
SamplerComparisonState shadowSampler : register(s10);






float4 CalculateAmbient(float4 matAmbient, float4 lightAmbient)
{
    return matAmbient * lightAmbient;
}


void CalculateDiffuseAndSpecular(
    float3 toLightW,
    float3 normalW,
    float3 toEyeW,
    float4 matDiffuse,
    float4 matSpec,
    float4 lightDiffuse,
    float4 lightSpec,
    float glossSample,
    inout float4 diffuseColor,
    inout float4 specularColor)
{

    // diffuse factor
    float Kd = dot(toLightW, normalW);

    [flatten]
    if (Kd > 0.0f)
    {
        // diffuse component
        diffuseColor = Kd * matDiffuse * lightDiffuse;

        // specular component
        float3 halfVectorW = normalize(toLightW + toEyeW);
        float exponent = exp2(9.0 * glossSample);
        float Ks = pow(max(dot(halfVectorW, normalW), 0.f), exponent);
        specularColor = Ks * matSpec * lightSpec;
    }
}


void ApplyAttenuation(
    float3 lightAttenuation,
    float distance,
    float falloff,
    inout float4 ambient,
    inout float4 diffuse,
    inout float4 specular)
{
    float attenuationFactor = 1.0f / dot(lightAttenuation, float3(1.0f, distance, distance * distance));
    ambient *= falloff;
    diffuse *= attenuationFactor * falloff;
    specular *= attenuationFactor * falloff;
}


void CalculateDirAndDistance(float3 pos, float3 target, out float3 dir, out float distance)
{
    float3 toTarget = target - pos;
    distance = length(toTarget);
    
    // now dir is normalize 
    toTarget /= distance;

    dir = toTarget;
}


float3 BumpNormalW(float2 uv, float3 normalW, float3 tangentW)
{
    float3 normalSample = normalTexture.Sample(textureSampler, uv).rgb;

    // remap the normal values inside the [-1,1] range from the [0,1] range
    float3 bumpNormalT = 2.f * normalSample - 1.f;

    // create the tangent space to world space matrix
    float3x3 TBN = float3x3(tangentW, cross(normalW, tangentW), normalW);

    return mul(bumpNormalT, TBN);
}




void DirectionalLightContribution(Material mat, DirectionalLight light, float3 normalW, float3 toEyeW, float glossSample, out float4 ambient, out float4 diffuse, out float4 specular)
{
    // default values
    ambient = float4(0.f, 0.f, 0.f, 0.f);
    diffuse = float4(0.f, 0.f, 0.f, 0.f);
    specular = float4(0.f, 0.f, 0.f, 0.f);


    float3 toLightW = -light.dirW;

    // shading componets
    ambient = CalculateAmbient(mat.ambient, light.ambient);
    CalculateDiffuseAndSpecular(toLightW, normalW, toEyeW, mat.diffuse, mat.specular, light.diffuse, light.specular, glossSample, diffuse, specular);

}


void PointLightContribution(Material mat, PointLight light, float3 posW, float3 normalW, float3 toEyeW, float glossSample, out float4 ambient, out float4 diffuse, out float4 specular)
{
    // default values
    ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    specular = float4(0.0f, 0.0f, 0.0f, 0.0f);


    float distance;
    float3 toLightW;
    CalculateDirAndDistance(posW, light.posW, toLightW, distance);

    
    // ealry rejection
    if (distance > light.range)
        return;
    

    float falloff = 1.f - (distance / light.range);
    
    // shading componets
    ambient = CalculateAmbient(mat.ambient, light.ambient);
    CalculateDiffuseAndSpecular(toLightW, normalW, toEyeW, mat.diffuse, mat.specular, light.diffuse, light.specular, glossSample, diffuse, specular);
    ApplyAttenuation(light.attenuation, distance, falloff, ambient, diffuse, specular);
}


void SpotLightContribution(Material mat, SpotLight light, float3 posW, float3 normalW, float3 toEyeW, float glossSample, out float4 ambient, out float4 diffuse, out float4 specular)
{
    // default values
    ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
    diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
    specular = float4(0.0f, 0.0f, 0.0f, 0.0f);

    float distance;
    float3 toLightW;
    CalculateDirAndDistance(posW, light.posW, toLightW, distance);


    // ealry rejection
    if (distance > light.range)
        return;


    // spot effect factor
    float spot = pow(max(dot(-toLightW, light.dirW), 0.0f), light.spot);

    // shading componets
    ambient = CalculateAmbient(mat.ambient, light.ambient);
    CalculateDiffuseAndSpecular(toLightW, normalW, toEyeW, mat.diffuse, mat.specular, light.diffuse, light.specular, glossSample, diffuse, specular);
    ApplyAttenuation(light.attenuation, distance, spot, ambient, diffuse, specular);
}

float CalculateLitFactor(float4 pos, Texture2D map)
{
    // shadows
    pos.xyz /= pos.w;
    float depthNDC = pos.z;
    
    uint width = 0;
    uint height = 0;
    map.GetDimensions(width, height);
    
    float xOffset = 1.0 / width;
    float yOffset = 1.0 / height;

    float litFactor = 0.f;

    // PCF with 3x3 kernel
    for (int y = -1; y <= 1; y++)
    {
        for (int x = -1; x <= 1; x++)
        {
            float2 location = float2(pos.x + xOffset * x, pos.y + yOffset * y);
            litFactor += map.SampleCmpLevelZero(shadowSampler, location, depthNDC).r;
        }
    }

    litFactor = litFactor / 9.f;
 
    return litFactor;
}

float CalculateLitFactorSimple(float4 pos, Texture2D map)
{
    // shadows
    pos.xyz /= pos.w;
    float depthNDC = pos.z;
    float litFactor = map.SampleCmpLevelZero(shadowSampler, pos.xy, depthNDC).r;

    return litFactor;
}



float4 main(VertexOut pin) : SV_TARGET
{
    pin.normalW = normalize(pin.normalW);

    // make sure tangentW is still orthogonal to normalW and is unit leght even
    // after the rasterizer stage (interpolation) 
    pin.tangentW = pin.tangentW - (dot(pin.tangentW, pin.normalW) * pin.normalW);
    pin.tangentW = normalize(pin.tangentW);


    // bump normal from texture
    float3 bumpNormalW;
    bumpNormalW = BumpNormalW(pin.uv, pin.normalW, pin.tangentW);
    

    float glossSample = glossTexture.Sample(textureSampler, pin.uv).r;
    float3 toEyeW = normalize(eyePosW - pin.posW);
    
    float4 totalAmbient = float4(0.f, 0.f, 0.f, 0.f);
    float4 totalDiffuse = float4(0.f, 0.f, 0.f, 0.f);
    float4 totalSpecular = float4(0.f, 0.f, 0.f, 0.f);
    float4 ambient;
    float4 diffuse;
    float4 specular;

    if (useDirLight)
    {
        float litFactor = CalculateLitFactor(pin.shadowPosH, shadowMap);
            
        [unroll]
        for (uint i = 0; i < DIRECTIONAL_LIGHT_COUNT; i++)
        {
            DirectionalLightContribution(material, dirLights[i], bumpNormalW, toEyeW, glossSample, ambient, diffuse, specular);
            totalAmbient += ambient;
            totalDiffuse += diffuse * litFactor;
            totalSpecular += specular * litFactor;
        }
    }

    if (usePointLight)
    {
        // shadows
        float litFactor = 1.f;

        [unroll]
        for (uint i = 0; i < 6; i++)
        {
            if (pin.shadowPosHPointLight[i].w < 0)
                continue;

            float tempLitFactor = 1.f;
            switch (i)
            {
                case 0:
                        tempLitFactor = CalculateLitFactor(pin.shadowPosHPointLight[i], pointLightMap_1);
                    break;
                case 1:
                        tempLitFactor = CalculateLitFactor(pin.shadowPosHPointLight[i], pointLightMap_2);
                    break;
                case 2:
                        tempLitFactor = CalculateLitFactor(pin.shadowPosHPointLight[i], pointLightMap_3);
                    break;
                case 3:
                        tempLitFactor = CalculateLitFactor(pin.shadowPosHPointLight[i], pointLightMap_4);
                    break;
                case 4:
                        tempLitFactor = CalculateLitFactor(pin.shadowPosHPointLight[i], pointLightMap_5);
                    break;
                case 5:
                        tempLitFactor = CalculateLitFactor(pin.shadowPosHPointLight[i], pointLightMap_6);
                    break;
                default:
                    break;
            }

            litFactor *= tempLitFactor;
        }
                
        [unroll]
        for (uint j = 0; j < POINT_LIGHT_COUNT; j++)
        {
            PointLightContribution(material, pointLights[j], pin.posW, bumpNormalW, toEyeW, glossSample, ambient, diffuse, specular);
            totalAmbient += ambient;
            totalDiffuse += diffuse * litFactor;
            totalSpecular += specular * litFactor;
        }

    }
    
    if (useSpotLight)
    {
        float litFactor = CalculateLitFactor(pin.shadowPosHSpotLight, shadowMapSpotLight);
        
        pin.shadowPosHSpotLight.xyz /= pin.shadowPosHSpotLight.w;
        float depthNDC = pin.shadowPosHSpotLight.z;
    
        float2 projectorUV;

        projectorUV.x = pin.shadowPosHSpotLight.x / 2.f + 0.5f;
        projectorUV.y = -pin.shadowPosHSpotLight.y / 2.f + 0.5f;
                
        
        [unroll]
        for (uint i = 0; i < 1; i++)
        {
            SpotLightContribution(material, spotLight, pin.posW, bumpNormalW, toEyeW, glossSample, ambient, diffuse, specular);
            float4 textureColor = projectorTexture.Sample(textureSampler, projectorUV.xy);

            totalAmbient += ambient;
            totalDiffuse += diffuse * textureColor * litFactor;
            totalSpecular += specular * textureColor * litFactor;
        }

    }
    

    float4 diffuseColor = diffuseTexture.Sample(textureSampler, pin.uv);
    float4 finalColor = diffuseColor * (totalAmbient + totalDiffuse) + totalSpecular;
    finalColor.a = diffuseColor.a * totalDiffuse.a;

    return finalColor;

}