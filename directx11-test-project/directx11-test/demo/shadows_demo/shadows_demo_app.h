#pragma once


#include <input/mouse.h>
#include <input/keyboard.h>
#include <camera/spherical_camera.h>
#include <mesh/mesh_generator.h>
#include <mesh/mesh_format.h>
#include <render/renderable.h>
#include <render/shading/render_pass.h>
#include <application/directx_app.h>


namespace xtest {
	namespace demo {

		/*
			Use F1, F2 and F3 to switch on/off the lights
			Use Spacebar to pause lights motion
			Usa ALT+Enter to switch full screen on/off
			Use F key to reframe the camera to the origin
			Use right mouse button to pan the view, left mouse button to rotate and mouse wheel to move forward
		*/

		class ShadowsDemoApp : public application::DirectxApp, public input::MouseListener, public input::KeyboardListener
		{
		public:


			struct DirectionalLight
			{
				DirectX::XMFLOAT4 ambient;
				DirectX::XMFLOAT4 diffuse;
				DirectX::XMFLOAT4 specular;
				DirectX::XMFLOAT3 dirW;
				float _explicit_pad_;
			};

			struct PointLight
			{
				DirectX::XMFLOAT4 ambient;
				DirectX::XMFLOAT4 diffuse;
				DirectX::XMFLOAT4 specular;
				DirectX::XMFLOAT3 posW;
				float range;
				DirectX::XMFLOAT3 attenuation;
				float _explicit_pad_;
			};

			struct SpotLight
			{
				DirectX::XMFLOAT4 ambient;
				DirectX::XMFLOAT4 diffuse;
				DirectX::XMFLOAT4 specular;
				DirectX::XMFLOAT3 posW;
				float range;
				DirectX::XMFLOAT3 dirW;
				float spot;
				DirectX::XMFLOAT3 attenuation;
				float _explicit_pad_;
			};

			struct Material
			{
				DirectX::XMFLOAT4 ambient;
				DirectX::XMFLOAT4 diffuse;
				DirectX::XMFLOAT4 specular;
			};

			struct PerObjectData
			{
				DirectX::XMFLOAT4X4 W;
				DirectX::XMFLOAT4X4 W_inverseTraspose;
				DirectX::XMFLOAT4X4 WVP;
				DirectX::XMFLOAT4X4 TexcoordMatrix;
				DirectX::XMFLOAT4X4 WVPT_shadowMap;
				DirectX::XMFLOAT4X4 WVPT_shadowMapSpotLight;
				DirectX::XMFLOAT4X4 WVPT_shadowMapPointLight[6];
				Material material;
			};

			struct PerObjectDataShadow
			{
				DirectX::XMFLOAT4X4 WVP;
			};

			static const int k_pointLightCount = 1;
			static const int k_dirLightCount = 1;
			struct PerFrameData
			{
				DirectionalLight dirLights[k_dirLightCount];
				PointLight pointLights[k_pointLightCount];
				SpotLight spotLight;
				DirectX::XMFLOAT3 eyePosW;
				float _explicit_pad_;
			};

			struct RarelyChangedData
			{
				int32 useDirLight;
				int32 usePointLight;
				int32 useSpotLight;
				int32 _explicit_pad_;
			};

			ShadowsDemoApp(HINSTANCE instance, const application::WindowSettings& windowSettings, const application::DirectxSettings& directxSettings, uint32 fps = 60);
			~ShadowsDemoApp();

			ShadowsDemoApp(ShadowsDemoApp&&) = delete;
			ShadowsDemoApp(const ShadowsDemoApp&) = delete;
			ShadowsDemoApp& operator=(ShadowsDemoApp&&) = delete;
			ShadowsDemoApp& operator=(const ShadowsDemoApp&) = delete;


			virtual void Init() override;
			virtual void OnResized() override;
			virtual void UpdateScene(float deltaSeconds) override;
			virtual void RenderScene() override;

			virtual void OnWheelScroll(input::ScrollStatus scroll) override;
			virtual void OnMouseMove(const DirectX::XMINT2& movement, const DirectX::XMINT2& currentPos) override;
			virtual void OnKeyStatusChange(input::Key key, const input::KeyStatus& status) override;

		private:

			void InitRenderTechnique();
			void InitRenderables();
			void InitLights();
			void InitShadows();
			PerObjectData ToPerObjectData(const render::Renderable& renderable, const std::string& meshName) const;
			PerObjectDataShadow ToPerObjectDataDirLight(const render::Renderable& renderable, const std::string& meshName) const;
			PerObjectDataShadow ToPerObjectDataSpotLight(const render::Renderable& renderable, const std::string& meshName) const;
			PerObjectDataShadow ToPerObjectDataPointLight(const render::Renderable& renderable, const std::string& meshName, const uint32 offset) const;

			DirectionalLight m_dirKeyLight;
			DirectionalLight m_dirFillLight;
			SpotLight m_spotLight;
			PointLight m_pointLight;
			RarelyChangedData m_lightingControls;
			bool m_isLightingControlsDirty;
			bool m_stopLights;

			Microsoft::WRL::ComPtr<ID3D11Texture2D> m_textureShadowsDirLight;
			Microsoft::WRL::ComPtr<ID3D11DepthStencilView> m_depthStencilViewShadowsDirLight;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shaderViewShadowsDirLight;

			Microsoft::WRL::ComPtr<ID3D11Texture2D> m_textureShadowsSpotLight;
			Microsoft::WRL::ComPtr<ID3D11DepthStencilView> m_depthStencilViewShadowsSpotLight;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shaderViewShadowsSpotLight;

			std::array<Microsoft::WRL::ComPtr<ID3D11Texture2D>, 6> m_texturePointLight;
			std::array<Microsoft::WRL::ComPtr<ID3D11DepthStencilView>, 6> m_depthStencilViewPointLight;
			std::array<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>, 6> m_shaderViewPointLight;

			render::shading::RenderPass m_dirLightRenderPass;
			render::shading::RenderPass m_spotLightRenderPass;
			render::shading::RenderPass m_drawRenderPass;
			std::array<render::shading::RenderPass, 6> m_pointLightRenderPass;

			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_projectorTexture;

			camera::SphericalCamera m_camera;
			camera::SphericalCamera m_boundingSphere;
			std::vector<render::Renderable> m_objects;


			std::shared_ptr<render::shading::VertexShader> m_vertexShader;
			std::shared_ptr<render::shading::PixelShader> m_pixelShader;

			float m_shadowResolution = 1024.f * 4;
			D3D11_VIEWPORT m_viewportShadows;
			std::shared_ptr<render::shading::VertexShader> m_vertexShaderShadows;
			std::shared_ptr<render::shading::PixelShader> m_pixelShaderShadows = nullptr;

		};

	} // demo
} // xtest


